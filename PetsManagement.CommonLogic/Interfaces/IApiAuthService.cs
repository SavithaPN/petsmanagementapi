﻿using PetsManagement.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetsManagement.CommonLogic.Interfaces
{
    public interface IApiAuthService
    {
        AuthResponseDto Authenticate(AuthInformationDto user);
    }
}
