﻿using PetsManagement.DataAccess.Contexts;
using PetsManagement.DataAccess.Migrations;
using System.Data.Entity;

namespace PetsManagement.DataAccess.Initializer
{
    public  class CustomInitializer : MigrateDatabaseToLatestVersion<PetsManagementDBContext, Configuration>
    {
        public override void InitializeDatabase(PetsManagementDBContext context)
        {
            base.InitializeDatabase(context);
        }

    }
}
