﻿using PetsManagement.Common.Dto;
using PetsManagement.Common.Dto.Enum;
using PetsManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetsManagement.CommonLogic.Interfaces
{
    public interface ICustomerService
    {
        Task<IEnumerable<Customer>> ListCustomers();
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddCustomer(Customer customer);
    }
}
