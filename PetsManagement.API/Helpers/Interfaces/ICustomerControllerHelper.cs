﻿using PetsManagement.Common.Dto;
using PetsManagement.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetsManagement.API.Helpers.Interfaces
{
    public interface ICustomerControllerHelper
    {
        Task<IEnumerable<CustomerDto>> ListCustomers();
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddCustomer(CustomerDto customerDto);
    }
}
