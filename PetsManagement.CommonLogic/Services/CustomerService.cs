﻿using PetsManagement.Common.Dto;
using PetsManagement.Common.Dto.Enum;
using PetsManagement.CommonLogic.Interfaces;
using PetsManagement.DataAccess.Repository.Interfaces;
using PetsManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetsManagement.CommonLogic.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _customerRepository = unitOfWork.GetRepository<Customer>();

        }

        /// <summary>
        /// Adds customer to the database
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddCustomer(Customer customer)
        {
            try
            {
                _customerRepository.Insert(customer);
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(customer.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(ex.Message);
                return result;
            }
        }

        /// <summary>
        /// Fetches all the customer from database
        /// </summary>
        /// <returns></returns>
        public  Task<IEnumerable<Customer>> ListCustomers()
        {
            try
            {
                return  _customerRepository.GetAsync();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;
            }
           
        }
    }
}
