﻿using PetsManagement.DataAccess.Contexts;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetsManagement.DataAccess.Initializer
{
    public class MigrationContextFactory : IDbContextFactory<PetsManagementDBContext>
    {
        public PetsManagementDBContext Create()
        {
            return new PetsManagementDBContext("PetsManagementDBContext");
        }
    }
}
