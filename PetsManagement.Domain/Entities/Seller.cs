﻿using PetsManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetsManagement.Domain.Entities
{
    public class Seller:IBasicTrackable
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SellerIdNumber1 { get; set; }
        public string SellerIdNumber2 { get; set; }
        public string LicenseNumber { get; set; }
        public string AdditionalInformation { get; set; }

        [Required]
        public virtual Address Address { get; set; }
        [Required]
        public virtual  ContactInformation ContactInfo { get; set; }

        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }
}
