﻿namespace PetsManagement.DataAccess.Migrations
{
    using PetsManagement.DataAccess.Contexts;
    using PetsManagement.Domain.Entities;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<PetsManagementDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
        private string DefaultDateFormat = "MM-dd-yyyy HH:mm:ss";
        private string DefaultCountry = "USA";
        private Guid CreatedBy;
        private Guid LastActionBy;
        private Guid DefaultCustomerId = new Guid("193da4f9-e3cd-4ae0-9b1b-28232c163b62");

        protected override void Seed(PetsManagementDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            CreatedBy = Guid.NewGuid();
            LastActionBy = Guid.NewGuid();
            CreateRolesAndUsers(new ApplicationIdentityDbContext(), context);
            
            AddDefaultCustomer(context);
        }

        private void CreateRolesAndUsers(ApplicationIdentityDbContext appContext, PetsManagementDBContext soContext)
        {
            var roleManager = new RoleManager<ApplicationUserRole>(new RoleStore<ApplicationUserRole>(appContext));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(appContext));

            if (!roleManager.RoleExists("Admin"))
            {
                //var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                var role = new ApplicationUserRole();
                role.Id = Guid.NewGuid().ToString();
                role.Name = "Admin";
                role.CreatedDate = DateTime.UtcNow;
                role.LastAction = DateTime.UtcNow;
                role.CreatedBy = CreatedBy;
                role.LastActionBy = LastActionBy;
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "admin";
                user.Email = "admin@brainmeshai.com";
                user.EmailConfirmed = true;
                user.CreatedDate = DateTime.UtcNow;
                user.LastAction = DateTime.UtcNow;
                user.RoleId = Guid.Parse(role.Id);
                user.Active = true;
                string adminPWD = "admin123";
                var adminUser = userManager.Create(user, adminPWD);

                if (adminUser.Succeeded)
                {
                    var result = userManager.AddToRole(user.Id, "Admin");
                }
            }
        }

        private void AddDefaultCustomer(PetsManagementDBContext context)
        {
            Customer oCustomer = new Customer
            {
                Id = DefaultCustomerId
            };

            if (context.Customers.Where(p => p.Id == oCustomer.Id).Count() <= 0)
            {
                oCustomer.CustomerName = "Default";
                oCustomer.Group = "Default";
                oCustomer.Active = true;
                oCustomer.Country = DefaultCountry;
                oCustomer.DateTimeFormat = DefaultDateFormat;
                oCustomer.Address = "US KY";
                oCustomer.State = "Kentucky";
                oCustomer.CreatedDate = DateTime.UtcNow;
                oCustomer.CreatedBy = CreatedBy;
                oCustomer.LastAction = DateTime.UtcNow;
                oCustomer.LastActionBy = LastActionBy;
                context.Customers.Add(oCustomer);
                context.SaveChanges();
            }
        }
    }
}
