﻿using AutoMapper;
using PetsManagement.API.Helpers.Interfaces;
using PetsManagement.Common.Dto;
using PetsManagement.Common.Dto.Enum;
using PetsManagement.CommonLogic.Interfaces;
using PetsManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PetsManagement.API.Helpers
{
    public class CustomerControllerHelper : ICustomerControllerHelper
    {
        ICustomerService _customerService;
        IMapper _mapper;
        public CustomerControllerHelper(
            ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddCustomer(CustomerDto customerDto)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<CustomerDto,Customer >();
            });
            _mapper = config.CreateMapper();
            Customer customer = _mapper.Map<Customer>(customerDto);
            return await _customerService.AddCustomer(customer);
        }

        public async Task<IEnumerable<CustomerDto>> ListCustomers()
        {
            var customers = await _customerService.ListCustomers();
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Customer, CustomerDto>();
            });
            _mapper = config.CreateMapper();
            List<CustomerDto> customerDtos = _mapper.Map<IEnumerable<CustomerDto>>(customers).ToList();
            return customerDtos;
        }
    }
}