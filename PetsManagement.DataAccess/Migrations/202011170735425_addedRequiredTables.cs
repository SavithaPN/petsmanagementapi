﻿namespace PetsManagement.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedRequiredTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SellerId = c.Guid(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        State = c.String(),
                        District = c.String(),
                        WardNumber = c.String(),
                        City = c.String(),
                        ZipCode = c.String(),
                        Country = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sellers", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Sellers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        SellerIdNumber1 = c.String(),
                        SellerIdNumber2 = c.String(),
                        LicenseNumber = c.String(),
                        AdditionalInformation = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContactInformations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SellerId = c.Guid(nullable: false),
                        HomePhoneNumber = c.String(),
                        MobileNumber = c.String(),
                        EmailId = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sellers", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.CategoryTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Offerings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        SellerId = c.Guid(nullable: false),
                        OfferDescription = c.String(),
                        NoOfItemsAvailable = c.Int(nullable: false),
                        ExpiryDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductImages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        Type = c.String(),
                        Image = c.Binary(),
                        SlNo = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        UnitsAvailable = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                        Category_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryTypes", t => t.Category_Id)
                .Index(t => t.Category_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductImages", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Products", "Category_Id", "dbo.CategoryTypes");
            DropForeignKey("dbo.Addresses", "Id", "dbo.Sellers");
            DropForeignKey("dbo.ContactInformations", "Id", "dbo.Sellers");
            DropIndex("dbo.Products", new[] { "Category_Id" });
            DropIndex("dbo.ProductImages", new[] { "ProductId" });
            DropIndex("dbo.ContactInformations", new[] { "Id" });
            DropIndex("dbo.Addresses", new[] { "Id" });
            DropTable("dbo.Products");
            DropTable("dbo.ProductImages");
            DropTable("dbo.Offerings");
            DropTable("dbo.CategoryTypes");
            DropTable("dbo.ContactInformations");
            DropTable("dbo.Sellers");
            DropTable("dbo.Addresses");
        }
    }
}
