﻿using PetsManagement.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetsManagement.Domain.Entities
{
    public class Product : IBasicTrackable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CategoryType Category { get; set; }

        public virtual ICollection<ProductImages> ProductImages { get; set; }
        public string Description { get; set; }
        public int UnitsAvailable { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }
}
