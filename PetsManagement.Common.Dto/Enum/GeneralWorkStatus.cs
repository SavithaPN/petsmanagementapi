﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetsManagement.Common.Dto.Enum
{
    public enum GeneralWorkStatus
    {
        Success = 1,
        ValidationFailed = 2,
        ExecutionFailed = 3
    }
}
