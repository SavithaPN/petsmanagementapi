﻿using PetsManagement.DataAccess.Migrations;
using PetsManagement.Domain.Entities;
using System.Data.Entity;

namespace PetsManagement.DataAccess.Contexts
{
    public class PetsManagementDBContext : DbContext
    {
        public PetsManagementDBContext(string conn): base(conn)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<PetsManagementDBContext, Configuration>());

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Customer>  Customers { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<CategoryType> CategoryTypes { get; set; }
        public DbSet<ContactInformation> ContactInformations { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductImages> ProductImages { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Offering> Offerings { get; set; }
    }
}
