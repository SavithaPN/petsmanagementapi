﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetsManagement.Domain.Interfaces
{
    public interface IBasicTrackable
    {
        DateTime CreatedDate { get; set; }
        Guid CreatedBy { get; set; }
        DateTime LastAction { get; set; }
        Guid LastActionBy { get; set; }
    }
}
