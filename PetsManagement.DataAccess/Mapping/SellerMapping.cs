﻿using PetsManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetsManagement.DataAccess.Mapping
{
    public class SellerMapping : EntityTypeConfiguration<Seller>
    {
        public SellerMapping()
        {
            //Table Name
            this.ToTable("Sellers");

            //Primary key
            this.HasKey(s => s.Id);
            this.Property(s => s.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.HasRequired(a => a.Address).WithRequiredPrincipal(s => s.Seller);
            this.HasRequired(c => c.ContactInfo).WithRequiredPrincipal(s => s.Seller);
        }
    }

    public class AddressMapping : EntityTypeConfiguration<Address>
    {
        public AddressMapping()
        {
            //table 
            ToTable("Addresses");

            // Primary Key
            this.HasKey(e => e.Id);
            this.Property(e => e.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }

    public class ContactInformationMapping : EntityTypeConfiguration<ContactInformation>
    {
        public ContactInformationMapping()
        {
            //Table Name
            this.ToTable("ContactInformations");

            //Primary Key
            this.HasKey(c => c.Id);
            this.Property(c => c.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }

    public class ProductImagesMapping : EntityTypeConfiguration<ProductImages>
    {
        public ProductImagesMapping()
        {
            //Table Name
            this.ToTable("ProductImages");

            //Primary Key
            this.HasKey(pi => pi.Id);
            this.Property(pi => pi.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.HasRequired(pi => pi.Product).WithMany(i => i.ProductImages).HasForeignKey(p => p.ProductId);
        }
    }

    public class OfferingMapping : EntityTypeConfiguration<Offering>
    {
        public OfferingMapping()
        {
            //Table Name
            this.ToTable("Offerings");

            //Primary key
            this.HasKey(s => s.Id);
            this.Property(s => s.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

        }
    }


}
